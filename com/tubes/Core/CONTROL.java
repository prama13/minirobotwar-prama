package com.tubes.Core;

import java.awt.Color;
import java.util.Map;

import com.tubes.Field.Field;
import com.tubes.Field.Game;
import com.tubes.Player.Player;

public final class CONTROL {
	public static final String DEFAULT_NAME = "DEFAULT";
	/**
	 * CURRENT_[Something] jadi pointer Global apa/siapa [Something] dalam
	 * Konteks Sekarang
	 * 
	 **/
	public static Player CURRENT_PLAYER = new Player();// NULL SAFE! :D
	public static Game CURRENT_GAME;
	public static Field CURRENT_FIELD;
	public static int CURRENT_LEVEL=0;
	


	public static String RESOURCE_PATH = "src/Dokumen/";
	public static String CURRENT_POINTER = CONTROL.CURRENT_PLAYER.getName();
	public static int V_SIZE = 1;
	public static int H_SIZE = 3;

	public static final class CONSOLE {
		public static final int WIDTH = 80;
		public static final Color Bg = Color.BLACK;
		public static final Color Fg = Color.green;

	}

	public static class FIELD {
		public static int HEIGHT = 5;
		public static int HIDDEN_LENGTH = FIELD.HEIGHT;
		public static int LENGTH = 15;

	}

	public static final class DROPRATE {
		private static final int CURRENCY = 100;
		public static final int LOW = ATTACK.LOW / ATTACK.LOW
				* DROPRATE.CURRENCY; // 100
		public static final int MEDIUM = ATTACK.MEDIUM / ATTACK.LOW
				* DROPRATE.CURRENCY;// 200
		public static final int HIGH = ATTACK.HIGH / ATTACK.LOW
				* DROPRATE.CURRENCY; // 400
		public static final int SUPER = ATTACK.SUPER / ATTACK.LOW
				* DROPRATE.CURRENCY; // 800
		public static final int ULTRA = ATTACK.ULTRA / ATTACK.LOW
				* DROPRATE.CURRENCY; // 1600

	}

	public static final class ATTACK {
		public static final int NULL = 0;
		public static final int LOW = 20;
		public static final int MEDIUM = 40;
		public static final int HIGH = 80;
		public static final int SUPER = 160;
		public static final int ULTRA = 320;
	}

	public static final class HEALTH {
		private static final int HitTurn = 3;
		public static final int LOW = ATTACK.LOW * HEALTH.HitTurn;
		public static final int MEDIUM = ATTACK.MEDIUM * HEALTH.HitTurn;
		public static final int HIGH = ATTACK.HIGH * HEALTH.HitTurn;
		public static final int SUPER = ATTACK.SUPER * HEALTH.HitTurn;
		public static final int ULTRA = ATTACK.ULTRA * HEALTH.HitTurn;
	}

	public static final class SPEED {
		public static final int LOW = FIELD.LENGTH / 10;
		public static final int MEDIUM = SPEED.LOW + 1;
		public static final int HIGH = SPEED.MEDIUM + 1;
		public static final int SUPER = SPEED.HIGH + 1;
	}

	public static final class PRICE {
		public static final int VERYCHEAP = DROPRATE.LOW / 5;
		public static final int CHEAP = DROPRATE.MEDIUM / 5;
		public static final int MEDIUM = DROPRATE.HIGH / 5;
		public static final int EXPENSIVE = DROPRATE.SUPER / 5;
		public static final int VERYEXPENSIVE = DROPRATE.ULTRA / 5;
	}

	public static final class ENERGY {
		public static final int VERYCHEAP = DROPRATE.LOW / 100;
		public static final int CHEAP = DROPRATE.MEDIUM / 100;
		public static final int MEDIUM = DROPRATE.HIGH / 100;
		public static final int PRICY = DROPRATE.HIGH / 50;
		public static final int LITTLEEXPENSIVE = DROPRATE.SUPER / 50;
		public static final int EXPENSIVE = DROPRATE.SUPER / 25;
		public static final int VERYEXPENSIVE = DROPRATE.ULTRA / 20;
	}

	

}
