package com.tubes.Core;

import com.tubes.util.SystemLog;

public abstract class Robot extends Entity {

	protected Robot() {
		super();
		SystemLog.print(getName()
				+"\n X:"	+getX()
				+"\n Y:"	+getY()
				+"\n LEFT:"	+getLeft()
				+"\n RIGHT:"	+getRight()
				+"\n BOTTOM:"	+getBottom()
				+"\n TOP:"	+getTop()
					);
	}

	protected Robot(int x, int y, int width, int height) {
		super(x, y, width, height);
		SystemLog.print(getName()
				+"\n X:"	+getX()
				+"\n Y:"	+getY()
				+"\n LEFT:"	+getLeft()
				+"\n RIGHT:"	+getRight()
				+"\n BOTTOM:"	+getBottom()
				+"\n TOP:"	+getTop()
				);
		
	}

	public abstract int getHealth();

	public abstract void setHealth(int i);

	public String getName() {

		return getClass().getSimpleName();
	}

	public void hurt(int damage) {
		setHealth(getHealth() - damage);

	}

	public void printHealth() {
		System.out.append("Health Of " + getName() + " is ").append(
				Integer.toString(getHealth()) + "\n");

		;
	}

}