package com.tubes.Core;

import java.awt.Point;

import com.tubes.Field.GameMap;
import com.tubes.Field.MapArrayProvider;
import com.tubes.util.SystemLog;

public abstract class Entity {

	// Atribut
	private Bound bound;

	protected Entity(int x, int y, int width, int height) {
		bound = new Bound(x, y, width, height);
	}

	public Entity() {
		super();
		bound = new Bound();
	}

	public boolean collideCheck(Entity Other) {
		return bound.checkCollision(Other.getBound());
	}

	// Method
	public void die() {
		try {
			finalize();
		} catch (Throwable e) {
			System.out.println("fail");
		}
	}

	public void fall() {
		//TODO
		int width = getWidth();
		int height =getHeight();
		int X = getLeft();
		int Y = getBottom();
		char[][] coor = GameMap.getMap(CONTROL.CURRENT_LEVEL).getMapChar();
		int HEIGHT= coor.length;
		int LENGTH=coor[0].length;
		int drawnX = LENGTH-getLeft() ;
		int drawnY = HEIGHT-getBottom();

		
		//MapArrayProvider.draw(coor);
		boolean kanjut = coor[drawnY][drawnX] == ' ';
		while (kanjut){
			moveAmount(0, -1);
			drawnY++;
			kanjut = coor[drawnY][drawnX] == ' ';
		}
		SystemLog.print("FALL, X="+X+"Y="+Y
				+"IndexCharX="+(drawnX)
				+"IndexCharY="+(drawnY)
				
				
				);

	}

	// Setter & Getter
	public int getHeight() {
		return bound.height;
	}

	public int getWidth() {
		
		return bound.width;
	}

	public void setHeight(int newheight) {
		bound.height = newheight;
	}

	public Bound getBound() {
		return bound;
	}

	public void setBound(Bound bound) {
		this.bound = bound;
	}

	public Point getLocation() {
		return bound.getLocation();
	}

	public void setLocation(int x, int y) {
		bound.setLocation(x, y);
	}

	public int getX() {
		return (int) bound.getX();
	}

	public int getY() {
		return (int) bound.getY();
	}

	public int getTop() {
		return bound.getTop() - 1;
	}

	public int getBottom() {
		return bound.getBottom();
	}

	public int getRight() {
		return bound.getRight();
	}

	public int getLeft() {
		return bound.getLeft();
	}

	public boolean checkCollision(Entity other) {
		return bound.checkCollision(other.getBound());
	}

	public void moveAmount(int dx, int dy) {
		bound.setLocation(getLeft() + dx, getTop() + 1 + dy);
	}

	public void moveTo(int x, int y) {
		bound.setLocation(x, y);
	}

	public void print() {
		System.out.print("Saya Entity Dalam lapangan");
	}
}