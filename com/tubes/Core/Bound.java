package com.tubes.Core;

import java.awt.Dimension;
import java.awt.Point;
import java.awt.Rectangle;

public class Bound extends Rectangle {

	public Bound(int x, int y, int l, int h) {
		super(x, y, l, h);
	}

	public Bound(int x, int y) {
		super(x, y);
	}

	public Bound(Point point) {
		super(point);
	}

	public Bound() {
		super();
	}

	public Bound(Dimension d) {

		super(d);
		// TODO Auto-generated constructor stub
	}

	private static final long serialVersionUID = 1L;
	private int bottom = y;
	private int top = bottom + height;
	private int left = x;
	private int right = left + width;

	public boolean checkCollision(Bound Other) {

		return checkCollisionHorizontal(Other) && checkCollisionVertical(Other);

	}

	public boolean checkCollisionHorizontal(Bound Other) {

		boolean checkleft = left >= Other.left && left <= Other.right;
		boolean checkright = right <= Other.left && right >= Other.right;

		return checkleft || checkright;
	}

	public boolean checkCollisionVertical(Bound Other) {

		boolean checktop = top >= Other.bottom && top <= Other.top;
		boolean checkbottom = Other.top >= bottom && bottom >= Other.bottom;

		// boolean checktop = ((Other.getY()-Other.getHeight()) - this.getX()) <
		// 0 ;
		// boolean checkbottom = ( Other.getY() - (this.getY()-this.getHeight())
		// ) > 0;

		return checktop || checkbottom;
	}

	public int getBottom() {
		return bottom;
	}

	public int getLeft() {
		return left;
	}

	public int getRight() {
		return right;
	}

	public int getTop() {
		return top;
	}

	public void print() {

		/*
		 * System.out.print ("height = "); System.out.println (height);
		 * 
		 * System.out.print ("width = "); System.out.println (width);
		 */

		// System.out.println ("Koordinat Titik Sudut: ");\
		/*
		 * System.out.println ("a: ("+left+","+top+")"); System.out.println
		 * ("b: ("+right+","+top+")"); System.out.println
		 * ("c: ("+left+","+bottom+")"); System.out.println
		 * ("d: ("+right+","+bottom+")"+"\n");
		 */
		System.out.println("Left: " + left);
		System.out.println("Right: " + right);
		System.out.println("Top: " + top);
		System.out.println("Bottom: " + bottom + "\n");

	}

	public void setLocation(int dx, int dy) {
		left = dx;
		right = dx + width;
		top = dy;
		bottom = dy - height;
	}
}
