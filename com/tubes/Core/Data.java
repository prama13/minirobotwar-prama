package com.tubes.Core;

import com.tubes.MiniRobot.type.Bomber;
import com.tubes.MiniRobot.type.Mentari;
import com.tubes.MiniRobot.type.Meriam;
import com.tubes.MiniRobot.type.Miring;
import com.tubes.MiniRobot.type.Perisai;
import com.tubes.MiniRobot.type.Senapan;
import com.tubes.MiniRobot.type.Tameng;
import com.tubes.util.FileArrayProvider;

public final class Data {
	private static String[] SPLASHSCREEN = FileArrayProvider.read("ASCII.txt");

	public static void START() {

		Data.Splashscreen();
		Data.Initiation();

	}

	public static void Initiation() {

		new Meriam();
		new Mentari();
		new Tameng();
		new Perisai();
		new Bomber();
		new Senapan();
		new Miring();

	}

	public static void Splashscreen() {
		FileArrayProvider.print(Data.SPLASHSCREEN, true);
		System.out.print("\n");
	}

}
