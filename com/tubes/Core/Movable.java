package com.tubes.Core;

public interface Movable {
	public void moveAmount(int dx, int dy);

	public void moveTo(int x, int y);

}