package com.tubes.Core;

import java.lang.reflect.Field;

import org.apache.commons.lang3.StringUtils;

public abstract class Information {
	protected boolean playable;

	public abstract String[] getElement();

	public String[] getFieldNames() {
		Class<?> info = getClass();
		Field[] field = info.getFields();
		String[] fieldNames = new String[field.length];
		for (int i = 0; i < field.length; i++)
			fieldNames[i] = field[i].getName();
		return fieldNames;
	}

	public void printFieldNames() {
		for (int i = 0; i < getFieldNames().length; i++)
			System.out.println("	- "
					+ StringUtils.capitalize(getFieldNames()[i]));
	}

	/* Getter playable */
	public boolean isPlayable() {
		if (playable == false)
			return false;
		else
			return true;
	}
}
