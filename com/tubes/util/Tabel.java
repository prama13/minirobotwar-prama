package com.tubes.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;

import com.tubes.Core.CONTROL.CONSOLE;
import com.tubes.Core.Information;

public class Tabel {

	/*
	 * How To Use: 1) Buat Instansiasi Tabel baru contoh: Tabel tabelPlayer =
	 * new Tabel.Builder().fromList(listPlayer).end(); *Kenapa pake Builder()?
	 * Itu cuma syntax aja, *Perhatikan!! kalo listPlayer bertipe
	 * ArrayList<PlayerInformation> 2) coba tabelPlayer.print();
	 */

	public static final int consoleWidth = CONSOLE.WIDTH - 5;
	public static int width;
	public ArrayList<Information> dataList;
	private Map<String, Integer> fieldNameMap = new HashMap<String, Integer>();
	private boolean invert = false;
	private String[] defaultFieldNames;

	public String[] getDefaultFieldNames() {
		return defaultFieldNames;
	}

	public void addFieldNames(String fieldName) {
		defaultFieldNames = ArrayUtils.add(defaultFieldNames, fieldName);
		fieldNameMap.put(fieldName, defaultFieldNames.length - 1);

	}

	public void Invert() {
		invert = true;
	}

	public static class Builder {
		private ArrayList<Information> fromList;

		public Builder fromList(ArrayList<?> dataList) {
			fromList = (ArrayList<Information>) dataList;
			return this;
		}

		public Tabel end() {
			Tabel tabel = new Tabel();
			tabel.dataList = fromList;
			tabel.defaultFieldNames = tabel.dataList.get(0).getFieldNames();
			for (int i = 0; i < tabel.defaultFieldNames.length; i++)
				tabel.fieldNameMap.put(tabel.defaultFieldNames[i], i);

			return tabel;
		}
	}

	private void Head() {
		Head(defaultFieldNames);
	}

	private void Body() {
		Body(defaultFieldNames);
	}

	private void Head(String[] names) {

		Class<?> info = dataList.get(0).getClass();
		info.getFields();
		String[] fieldNames = defaultFieldNames;
		String[] fieldNamesChosen = new String[names.length];

		for (int i = 0; i < names.length; i++)
			fieldNamesChosen[i] = "|"
					+ StringUtils.center(StringUtils
							.upperCase(fieldNames[fieldNameMap.get(names[i])]),
							(Tabel.consoleWidth - 2 - fieldNamesChosen.length)
									/ fieldNamesChosen.length);

		String head = StringUtils.join(fieldNamesChosen, "");
		Tabel.width = head.length() + 1;

		System.out.append(StringUtils.repeat("=", Tabel.width) + "\n")
				.append(head + "|\n")
				.append(StringUtils.repeat("=", Tabel.width) + "\n");
	}

	public void Body(String[] names) {

		String[] bodyNamesChosen = new String[names.length];
		for (Information i : dataList) {
			boolean a = i.isPlayable();
			if (invert)
				a = !a;
			if (a) {
				for (int j = 0; j < names.length; j++)
					bodyNamesChosen[j] = "|"
							+ StringUtils
									.center(i.getElement()[fieldNameMap
											.get(names[j])],
											(Tabel.consoleWidth - 1 - bodyNamesChosen.length)
													/ bodyNamesChosen.length);
				String body = StringUtils.join(bodyNamesChosen, "");
				System.out.append(body + "|\n");
			}
		}

		System.out.append(StringUtils.repeat("=", Tabel.width) + "\n");
	}

	public void print(String[] names) {

		Head(names);
		Body(names);

	}

	public void print() {

		Head();
		Body();

	}
}
