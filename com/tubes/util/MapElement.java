package com.tubes.util;

import java.util.Iterator;
import java.util.Map;

public class MapElement {
	public static void print(Map<String, String> propTest) {
		Iterator iterator = propTest.entrySet().iterator();
		while (iterator.hasNext()) {
			Map.Entry mapEntry = (Map.Entry) iterator.next();
			System.out.println("The key is: " + mapEntry.getKey()
					+ ",value is :" + mapEntry.getValue());
		}
	}
}
