package com.tubes.util;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import com.tubes.Core.CONTROL;

public class SystemLog {
	private static ArrayList<String> logList = new ArrayList<String>();
	private static int ID;
	
	public static void print (String logs) {
		logList.add(logs);
	}

	public static void print (int logs) {
		logList.add(Integer.toString(logs));
	}
	public static void print (float logs) {
		logList.add(Float.toString(logs));
	}
	public static void print (double logs) {
		logList.add(Double.toString(logs));
	}


	public static void println (String logs) {
		print(logs+"\n");
	}
	public static void println (int logs) {
		print(logs+"\n");
	}
	public static void println (float logs) {
		print(logs+"\n");
	}
	public static void println (double logs) {
		print(logs+"\n");
	}
	
	public static void flush () {
		PrintWriter out = null;
		try {
			out = new PrintWriter(new FileWriter(CONTROL.RESOURCE_PATH+"Log"+System.currentTimeMillis()+".txt"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}  
		  
		String[] logArray = new String[logList.size()];
		// Write each string in the array on a separate line  
		for (String s : logList.toArray(logArray)) {  
		    out.println(s);  
		}  
		  
		out.close();  
		ID++;
	}
	public static void debug() {
		String[] logArray = new String[logList.size()];
		for (String s : logList.toArray(logArray)) {  
		    System.out.println(s);  
		}  
	}

	

}
