package com.tubes.util;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.tubes.CLI.Konsol;
import com.tubes.Core.CONTROL;

public class FileArrayProvider {
	/*
	 * Buat Opak :D Contoh Code buat baca file TXT yang disimpen dalem String []
	 * dan bisa di print persis sama kaya di filenya Testnya ada di
	 * com/tubes/test dan ASCII.txt nya ada di folder Dokumen
	 */

	public static String[] read(String filename) {
		filename = CONTROL.RESOURCE_PATH + filename;
		try {
			FileReader fileReader = new FileReader(filename);
			BufferedReader bufferedReader = new BufferedReader(fileReader);

			List<String> lines = new ArrayList<String>();
			String line = null;
			while ((line = bufferedReader.readLine()) != null)
				lines.add(line);

			bufferedReader.close();

			return lines.toArray(new String[lines.size()]);
		} catch (IOException ioe) {
			return null;
			// Handle exception here, most of the time you will just log it.
		}
	}

	public static void print(String[] StringSequence, boolean center) {
		if (!center)
			for (String line : StringSequence) {
		
				System.out.println(line);
			}
		else
			for (String line : StringSequence) {

				System.out.println(StringUtils.center(line,
						CONTROL.CONSOLE.WIDTH - 1));
			}
	}

}
