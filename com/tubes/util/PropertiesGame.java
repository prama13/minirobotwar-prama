package com.tubes.util;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;

public class PropertiesGame {
	public String fileName;
	public Map<String, String> property;

	public PropertiesGame(Map<String, String> property, String fileName) {
		super();

		this.fileName = fileName;
		this.property = property;
	}

	public void save() {
		Properties prop = new Properties();

		try {

			Iterator iterator = property.entrySet().iterator();
			while (iterator.hasNext()) {
				Map.Entry mapEntry = (Map.Entry) iterator.next();
				prop.setProperty(mapEntry.getKey().toString(), mapEntry
						.getValue().toString());

			}

			// save properties to project root folder
			prop.store(new FileOutputStream("src/Dokumen/" + fileName
					+ ".properties"), null);

		} catch (IOException ex) {
			System.out.println("properties not found");
		}

	}

	public void print() {
		Properties prop = new Properties();

		try {
			Iterator iterator = property.entrySet().iterator();
			prop.load(new FileInputStream(fileName + ".properties"));

			// get the property value and print it out
			while (iterator.hasNext()) {
				Map.Entry mapEntry = (Map.Entry) iterator.next();
				String propString = prop.getProperty(mapEntry.getKey()
						.toString());
				System.out.println(propString);
			}

		} catch (IOException ex) {
			System.out.println("gagal");
		}

	}

	public Map<String, String> load() throws Exception {

		FileReader reader = new FileReader(fileName + ".properties");

		Properties prop = new Properties();
		try {
			prop.load(reader);
		} finally {
			reader.close();
		}
		Map<String, String> myMap = new HashMap<String, String>();
		for (Object key : prop.keySet())
			myMap.put(key.toString(), prop.get(key).toString());
		return myMap;
	}

}
