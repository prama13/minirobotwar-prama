package com.tubes.Field;

import com.tubes.Player.Energy;
import com.tubes.Player.Player;

public class Game {
	private Field field;
	private Player player;
	private Energy energy;
	private int turn;
	
	public Game(Field field, Player player, int turn){
		this.field = field;
		this.player = player;
		this.turn = turn;
	}

	public void nextTurn (){
		
		/* Phase Player
		 * 1. game add energy 		-OK
		 * 2. add/remove minirobot 	-OK
		 * 3. shoot()				-OK
		*/
		energy.add(field.getAllEnergy());//1
		
		field.attMiniRobot(); //3
		
		
		/* Phase Enemy
		 * 1. move		-OK
		 * 2. summon 	
		 * 3. shoot() 	-OK
		*/
		field.moveGiantRobot();//1
		
		field.attGiantRobot();//3
		
		turn++;
	}
	

}