package com.tubes.Field;


import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

import org.apache.commons.lang3.StringUtils;

import com.tubes.Core.CONTROL;

public class MapArrayProvider {
	/*
	 * Buat Opak :D
	 * Contoh Code buat baca file TXT yang disimpen dalem String [] 
	 * dan bisa di print persis sama kaya di filenya
	 * Testnya ada di com/tubes/test dan ASCII.txt nya ada di folder Dokumen
	 */
	private static String[] mapTerrain;
	
	public static String[] readLines(String filename)  {
	
		FileReader fileReader = null;
		try {
			fileReader = new FileReader(CONTROL.RESOURCE_PATH+filename);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		BufferedReader bufferedReader = new BufferedReader(fileReader);


		List<String> lines = new ArrayList<String>();
		String line = null;
		try {
			while ((line = bufferedReader.readLine()) != null)
				lines.add(StringUtils.strip(line));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		try {
			bufferedReader.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		mapTerrain = lines.toArray(new String[lines.size()]);
		return mapTerrain; 
	}
		
	public static void print()	{
	
		for(String str :mapTerrain){
			/** ARCHIEVED

str = StringUtils.replaceChars(str, "a", " ");
			str = StringUtils.replaceChars(str, "g", "_");
			 **/
			System.out.println(str);
		}
	}
	public static void draw(char[][] GRID) throws ArrayIndexOutOfBoundsException {
		
		int HEIGHT=GRID.length;
		int LENGTH=GRID[0].length;
		/**Header**/
		System.out
		.println(StringUtils.repeat("=",CONTROL.H_SIZE*LENGTH));

		for (int i=0;i<LENGTH;i++)
			System.out
			.append(StringUtils.center(Integer.toString(LENGTH-i),CONTROL.H_SIZE))
			;
		System.out
		.println("\n"+StringUtils.repeat("=",CONTROL.H_SIZE*LENGTH));
		/**Header**/
		
		for (int y = 0; y < HEIGHT; y++) {
			System.out.print("");
			for (int x = 0; x < LENGTH; x++)
				System.out.print(StringUtils.repeat(GRID[y][x],CONTROL.H_SIZE));
			System.out.print(StringUtils.repeat("| "+(HEIGHT-y)+"\n",1));
			System.out.print(StringUtils.repeat(
					StringUtils.leftPad(
							"| \n", (LENGTH+1)*CONTROL.H_SIZE),
							CONTROL.V_SIZE-1)
					);
		}

		/**Footer**/
		System.out
		.println(StringUtils.repeat("=",CONTROL.H_SIZE*LENGTH));
		/**Footer**/

	}

	public static class util{
		static char[][] cloneMapChar(char[][] original){
			  if (original == null) {
			        return null;
			    }

			    final char[][] result = new char[original.length][];
			    for (int i = 0; i < original.length; i++) {
			        result[i] = Arrays.copyOf(original[i], original[i].length);
			        // For Java versions prior to Java 6 use the next:
			        // System.arraycopy(original[i], 0, result[i], 0, original[i].length);
			    }
			    return result;
		}
	}
	/** ARCHIEVED

	public static void main(String[] args) throws IOException {
     	String path = MapArrayProvider.class.getResource("level1.txt").toString();
        System.out
        .append(MapArrayProvider.class.getResource("level1.txt").toString()+"\n")
        .append(MapArrayProvider.class.getResource("level1.txt").getPath()+"\n")
        .append(MapArrayProvider.class.getResource("level1.txt").getFile());
     
     	
        String[] lines = MapArrayProvider.readLines("level0.txt");
       // for (String line : lines) {
        //    System.out.println(line);
       // }
        
        MapArrayProvider.print();
        
	}
**/

}

