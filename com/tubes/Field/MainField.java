package com.tubes.Field;

import java.awt.Color;
import java.util.Scanner;

import com.tubes.CLI.Konsol;
import com.tubes.Core.CONTROL;
import com.tubes.GiantRobot.type.Elpijizoid;
import com.tubes.GiantRobot.type.GRD2;
import com.tubes.GiantRobot.type.GRD3;
import com.tubes.GiantRobot.type.GRU1;
import com.tubes.GiantRobot.type.GRU2;
import com.tubes.GiantRobot.type.Genderuwo;
import com.tubes.MiniRobot.type.Mentari;
import com.tubes.MiniRobot.type.Meriam;
import com.tubes.util.SystemLog;

import enigma.loaders.ConsoleProxyLoader;
import enigma.shells.commandline.CommandLineShell;

public class MainField {
	

	public static void main(String[] args) {
		Konsol.color(Color.green);
		Field fieldTest = new Field(1);
		fieldTest.debug();
		
		
		fieldTest.addMiniRobot(new Meriam(1,1));
		fieldTest.addMiniRobot(new Meriam(1,2));
		fieldTest.addMiniRobot(new Meriam(1,3));
		fieldTest.addMiniRobot(new Mentari(2,1));
		
		
		fieldTest.addGiantRobot(new Elpijizoid(13,4));
		fieldTest.addGiantRobot(new Genderuwo(12,1));
		fieldTest.addGiantRobot(new GRD2(9,4));
		
		fieldTest.draw();
		SystemLog.flush();
	//	SystemLog.debug();
		
		


		

try {	
		Scanner input = new Scanner(System.in);
		boolean go=true;
		while(go) {
			
			fieldTest.moveGiantRobot();
			fieldTest.printGiantRobot();
			input.nextLine();
			fieldTest.attGiantRobot();
			fieldTest.printMiniRobot();
			input.nextLine();
			fieldTest.getAllEnergy();
			fieldTest.attMiniRobot();
			fieldTest.printGiantRobot();
			input.nextLine();
			fieldTest.draw();
			go = (fieldTest.giantRobotList.size()!=0);
		
			
			
		}
		System.out.println("MainField.main()");
		System.err.println(fieldTest.playerDamage);
		} catch (ArrayIndexOutOfBoundsException e) {
			e.printStackTrace();
			SystemLog.print(e.toString()); 
			SystemLog.flush();
		}
		 





		
	}
}
