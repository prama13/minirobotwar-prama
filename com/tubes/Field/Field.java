package com.tubes.Field;
//TODO Jadiin Singleton?
import java.awt.Color;
import java.awt.Point;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.lang.String;
import java.lang.Character;

import org.apache.commons.lang3.StringUtils;

import com.tubes.CLI.Konsol;
import com.tubes.Core.CONTROL;
import com.tubes.Core.Robot;
import com.tubes.GiantRobot.GiantRobot;
import com.tubes.GiantRobot.type.Elpijizoid;
import com.tubes.GiantRobot.type.Genderuwo;
import com.tubes.MiniRobot.AttackDirection;
import com.tubes.MiniRobot.AttackerMiniRobot;
import com.tubes.MiniRobot.EnergizerMiniRobot;
import com.tubes.MiniRobot.MiniRobot;
import com.tubes.MiniRobot.type.Meriam;
import com.tubes.Player.Energy;
import com.tubes.Player.Sparepart;

public class Field{
	private int level;
	public int getLevel() {
		return level;
	}

	private GameMap gameMap;
	ArrayList<MiniRobot> miniRobotList = new ArrayList<MiniRobot>();
	ArrayList<GiantRobot> giantRobotList = new ArrayList<GiantRobot>();
	private Sparepart sparepart;
	public int playerDamage=0;
	private boolean debug = false;
	
	public void debug() {
		this.debug=true;
	}
	
	
public Field(int dlevel, ArrayList<MiniRobot> kerdil, ArrayList<GiantRobot> raksasa){
		this(dlevel);
		this.miniRobotList = kerdil;
		this.giantRobotList = raksasa;
		CONTROL.CURRENT_LEVEL = dlevel; 
}

public Field(int dlevel){
	this.level=dlevel;
	this.gameMap = GameMap.getMap(dlevel);
	CONTROL.CURRENT_LEVEL = dlevel; 
	
}

	
	public void addMiniRobot (MiniRobot minirobot)
	
	{
				
				miniRobotList.add(minirobot);	
			
	}
	public void addGiantRobot (GiantRobot giantrobot){
		//System.out.println("Field.addGiantRobot()");
		
		giantrobot.fall();
		giantRobotList.add(giantrobot);
		//MapArrayProvider.draw(CONTROL.CURRENT_MAP_GRID);
		//System.out.println("giantrobot.fall()");
		
	}
	
	public void delMiniRobot (MiniRobot minirobot){
		miniRobotList.remove(minirobot);
	}
	public void delGiantRobot (GiantRobot giantrobot){
		giantRobotList.remove(giantrobot);
	}
	public void resetMiniRobot (){
		miniRobotList.clear();
	}
	public void resetGiantRobot (){
		giantRobotList.clear();
	}
	public boolean IsEmptyMiniRobot (){
		return (miniRobotList.isEmpty());
	}
	public boolean IsEmptyGiantRobot (){
		return giantRobotList.isEmpty();
	}
	public void setMiniRobot(int index , MiniRobot minirobot){
		miniRobotList.set(index, minirobot);
	}
	public void setGiantRobot(int index , GiantRobot giantrobot){
		giantRobotList.set(index, giantrobot);
	}
	public MiniRobot getMiniRobot (int index){
		return miniRobotList.get(index);
	}
	public Robot getGiantRobot (int index){
		return giantRobotList.get(index);
	}
	public void moveGiantRobot(){
		
		ArrayList<GiantRobot> Error = new ArrayList<GiantRobot>();
		for (GiantRobot giant : giantRobotList) 
		{	
			
			int speed = giant.getSpeed();
			boolean stop = false;
			int LENGTH = GameMap.getMap(level).getLengthOfMap();
			while (!stop)
				for(MiniRobot mini:miniRobotList)
					if (mini.getBottom() == giant.getBottom())
						if (mini.getLeft() == giant.getLeft()-giant.getWidth())
							stop=true;
						else {
							for (int i=0;i<speed;i++)
							giant.moveAmount(-i,0);
							stop = true;
						}
						
			
		
			int drawnX = LENGTH-giant.getLeft() ;
			int width = giant.getWidth();
			if(drawnX+width >= 15 || giant.getHealth() <= 0)
				Error.add(giant);
			else
			giant.fall();
		} 
		for (GiantRobot giant:Error) {
		delGiantRobot(giant);
		playerDamage++;
		if (debug)
			System.err.println(giant.getName()+"has attacked playe, Player Damage="+playerDamage);
		}
	if(debug) 
	{
		System.out.println("Field.moveGiantRobot()");
		draw();
	}
	}


	public void attGiantRobot(){
		//TODO Ngaco abis, harus di coding ulang
		int giantX,giantY;
		Integer idxMiniRobot = null; 
		ArrayList<MiniRobot> Dead = new ArrayList<MiniRobot>();
		for (GiantRobot giant : giantRobotList) {
			
			giantX = giant.getLeft();
			giantY = giant.getBottom();

			for(int idx=0;idx<miniRobotList.size();idx++){
				Robot mini = miniRobotList.get(idx);
				if(mini.getBottom() == giantY)
				{
					if(mini.getLeft() == giant.getLeft()-giant.getWidth() || mini.getLeft() == giant.getLeft()-giant.getWidth()-1  )
						idxMiniRobot=idx;
				}
			}
			if (idxMiniRobot != null) {
					miniRobotList.get(idxMiniRobot).hurt(giant.getDamage());
					if(miniRobotList.get(idxMiniRobot).getHealth()<=0)
						Dead.add(miniRobotList.get(idxMiniRobot));
				}

			}
		for (MiniRobot mini:Dead)
			delMiniRobot(mini);
		if (debug) {
			System.out.println("Field.attGiantRobot()");
		}
		}
	
	public void getAllEnergy() {
		Energy energy = new Energy(0);
		
		
		for (Robot mini: miniRobotList)
		{
			EnergizerMiniRobot energizer = (EnergizerMiniRobot) mini;
			energy.add(energizer.genEnergy());
		}
		
		
	}
	
	public void attMiniRobot(){
		int x,y;
		Integer idxGiantRobot = null; 
		
		for(MiniRobot mini:miniRobotList){

			AttackerMiniRobot attacker = (AttackerMiniRobot) mini;
			x = attacker.getLeft();
			y = attacker.getBottom();
			
			AttackDirection direction = attacker.shoot();
			switch (direction)
			{
			case HORIZONTAL:
				for(int idx=0;idx<giantRobotList.size();idx++){
					Robot giant = giantRobotList.get(idx);
				
						if(y >= giant.getBottom() && y<= giant.getTop()){
							if(giant.getBottom()>= x||giant.getX()<=CONTROL.FIELD.LENGTH)
								idxGiantRobot =idx;
						}
					}
				
				if (idxGiantRobot != null)
				giantRobotList.get(idxGiantRobot).hurt(attacker.getAttack());
				

				idxGiantRobot = null;
				break;

			case VERTICAL:
				for(int idx=0;idx<giantRobotList.size();idx++){
					Robot giant = giantRobotList.get(idx);
					if(giant.getX() == x)
					{
						if(giant.getY()>y)
							idxGiantRobot=idx;
					}
				}
				if (idxGiantRobot != null)
					
						giantRobotList.get(idxGiantRobot).hurt(attacker.getAttack());
				
				break;
			case DIAGONAL:
				int xcheck = x;
				int ycheck = y;
				for(int idx=0;idx<giantRobotList.size();idx++){
					Robot giant = giantRobotList.get(idx);
					while((xcheck <CONTROL.FIELD.LENGTH) && (ycheck <CONTROL.FIELD.HEIGHT )){
						if(giant.getX() == xcheck && giant.getY() == ycheck){
							idxGiantRobot=idx;
						}
						xcheck++;
						ycheck++;
					}
					break;
				}
			
			}
			
		}
		ArrayList<GiantRobot> Dead = new ArrayList<GiantRobot>();
		for (GiantRobot giant : giantRobotList) 
		{
			if (giant.getHealth()<=0)
				Dead.add(giant);
		}
		for (GiantRobot giant:Dead)
			delGiantRobot(giant);
		
		if (debug) {
			System.out.println("Field.attMiniRobot()");
		}
	}
	public void printAllEntity() {
		printMiniRobot();
		printGiantRobot();
	}
	public void printMiniRobot(){
		int i = 1;
		int x,y,w,h;

		for (MiniRobot mini: miniRobotList)
		{
			Konsol.color(Color.CYAN);
			
			x = mini.getLeft();
			y = mini.getBottom();
			w = mini.getWidth();
			h = mini.getHeight();
			System.out
			.append(i+") " + mini.getName())
			.append("	("+x+","+y+")")
			.append("["+w+","+h+"]")
			
			.append(">> "+Integer.toString(mini.getHealth()) +"\n");
			i++;
			Konsol.reset();
		}
	
		
		
	}
	public void printGiantRobot() {
		int i = 1;
		int x,y,w,h;
		for (GiantRobot giant: giantRobotList)
		{	Konsol.color(Color.ORANGE);
			x = giant.getLeft();
			y = giant.getBottom();
			w = giant.getWidth();
			h = giant.getHeight();
			System.out
			.append(i+") " + giant.getName())
			.append("		("+x+","+y+")")
			.append("["+w+","+h+"]")
			.append(">> "+Integer.toString(giant.getHealth()) +"\n");
			i++;
			Konsol.reset();
		}
		
	}
	public void draw(){

		char[][] manipulatedChar = GameMap.getMap(level).getMapChar();
		int LENGTH = GameMap.getMap(level).getLengthOfMap();
		int HEIGHT = GameMap.getMap(level).getHeightOfMap();

		for(MiniRobot mini:miniRobotList){
			int drawnX = LENGTH-mini.getLeft();
			int drawnY = HEIGHT-mini.getBottom();
			manipulatedChar[drawnY][drawnX]='<'; 

		}

		//TODO GiantRobot
		for(GiantRobot giant:giantRobotList)
		{
			int width = giant.getWidth();
			int height = giant.getHeight();
			int Y = giant.getBottom();
			int drawnX = LENGTH-giant.getLeft() ;
			int drawnY = HEIGHT-giant.getBottom();
			char g = giant.getClass().getSimpleName().toUpperCase().charAt(0);
			
			for (int dx = 0; dx < width; dx++) {
				for (int dy = 0; dy < height; dy++) {
					manipulatedChar[drawnY-dy][drawnX+dx]=g;
					
				}
				
			} 
				
		}
	//15:00	CONTROL.CURRENT_MAP_GRID=fieldChar;
	if (debug) {
		printAllEntity();
	}
		GameMap.util.draw(manipulatedChar,debug,level);
	
		manipulatedChar=null;
	}
	
	
}
