package com.tubes.Field;

import java.io.FileNotFoundException;
import java.io.FileReader;

import com.tubes.Core.CONTROL;
import com.tubes.MiniRobot.MiniRobot;

public class FieldProxy{


	public static boolean getField(int level) {
		try {
			FileReader fileReader = new FileReader(CONTROL.RESOURCE_PATH+"level"+level+".txt");
			CONTROL.CURRENT_FIELD = new Field(level);
			return true;
		}catch (FileNotFoundException f) {
			System.err.println("Level Tidak ada, silahkan coba lagi");
			return false;
		}
		
	}
	
	
	public static void addMiniRobot(MiniRobot minirobot) {
		//TODO
	}

	public static void delMiniRobot(int x, int y) {
		CONTROL.CURRENT_FIELD.delMiniRobot(selectMiniRobot(x, y));
	}


	
	public static MiniRobot selectMiniRobot(int x, int y) {
		 MiniRobot selected=null;
		for(MiniRobot mini:CONTROL.CURRENT_FIELD.miniRobotList)
		{
			if (mini.getLeft()==x && mini.getBottom()==y) {
				selected = mini;
			}
		}
		
		if (selected!=null) {
			return selected;
			
		} else {
			System.err.println("MiniRobot tidak ditemukan");
			return null;
		}
				
				
	}

}
