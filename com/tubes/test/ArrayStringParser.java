package com.tubes.test;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import org.apache.commons.lang3.StringUtils;

public class ArrayStringParser {
	private String[] rules;

	public void setRules(String[] rules) {
		this.rules = rules;
	}

	public ArrayStringParser(String[] rules) {
		this.rules = rules;
	}

	public ArrayStringParser() {
		rules = null;
	}

	private boolean check(String a) {
		boolean check = false;
		int i = 0;
		if (rules != null) {
			while (check == false && i < rules.length) {
				check = StringUtils.containsIgnoreCase(a, rules[i])
						|| StringUtils.containsIgnoreCase(a, "exit");
				i++;
			}
			return check;
		}
		return true;
	}

	public String[] start(String pointer) {
		Scanner input = new Scanner(System.in);
		List<String> inputString = new ArrayList<String>();
		String a = null;
		while (!StringUtils.contains(a, "exit")) {

			System.out.print(pointer + "> ");
			a = input.nextLine().toLowerCase();
			if (check(a))
				inputString.add(a);
			else
				System.out.println("Field Not Found");

		}

		System.out.println(StringUtils.repeat("\n", 2));
		inputString.remove("exit");
		String[] names = new String[inputString.size()];
		inputString.toArray(names);
		return names;
	}
}
