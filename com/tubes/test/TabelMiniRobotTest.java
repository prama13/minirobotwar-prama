package com.tubes.test;

import com.tubes.Core.Data;
import com.tubes.MiniRobot.MiniRobot;
import com.tubes.util.Tabel;

public class TabelMiniRobotTest {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		Data.Initiation();
		Tabel tabelMiniRobot = new Tabel.Builder().fromList(MiniRobot.types)
				.end();
		/*
		 * System.out.println("Available Fields:"); String[] rules =
		 * ((MiniRobotInformation)
		 * tabelMiniRobot.dataList.get(0)).getFieldNames();
		 * ((MiniRobotInformation)
		 * tabelMiniRobot.dataList.get(0)).printFieldNames(); String [] names =
		 * new ArrayStringParser(rules).start("MiniRobot");
		 */

		// tabelMiniRobot.addFieldNames("cost");
		tabelMiniRobot.Invert();
		tabelMiniRobot.print();

	}
}
