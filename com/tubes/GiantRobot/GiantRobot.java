package com.tubes.GiantRobot;

import com.tubes.Core.Movable;
import com.tubes.Core.Robot;
import com.tubes.util.SystemLog;

public abstract class GiantRobot extends Robot implements Movable {

	protected GiantRobot(int x, int y, int width, int height) {
		super(x, y, width, height);
		
	}

	public abstract int getDamage(); // Static

	protected abstract int getSparepartdrop(); // Static

	public abstract int getSpeed(); // Static

	public void move() {
		int x = getLeft();
		int y = getBottom();
		moveAmount(x + getSpeed(), y);
	}

}