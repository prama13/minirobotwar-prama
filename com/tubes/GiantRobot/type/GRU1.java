package com.tubes.GiantRobot.type;

import com.tubes.Core.CONTROL.ATTACK;
import com.tubes.Core.CONTROL.DROPRATE;
import com.tubes.Core.CONTROL.HEALTH;
import com.tubes.Core.CONTROL.SPEED;
import com.tubes.Core.Movable;
import com.tubes.GiantRobot.GiantRobot;

public final class GRU1 extends GiantRobot implements Movable {

	private static final int width = 1;
	private static final int height = 2;
	private static final int damage = ATTACK.LOW;
	private static final int speed = SPEED.MEDIUM;
	private static final int defend = HEALTH.MEDIUM;
	private static final int Sparepartdrop = DROPRATE.MEDIUM;
	private int health = GRU1.defend;

	public GRU1(int x, int y) {
		super(x, y, GRU1.width, GRU1.height);
	}

	public GRU1() {
		super(0, 0, GRU1.width, GRU1.height);
	}

	public final int getDamage() {
		return GRU1.damage;
	}

	public final int getSparepartdrop() {
		return GRU1.Sparepartdrop;
	}

	@Override
	public final int getSpeed() {
		return GRU1.speed;
	}

	public int getHealth() {
		return health;
	}

	public void setHealth(int health) {
		this.health = health;
	}

	public void setLocation(int x, int y) {
	}
}
