package com.tubes.GiantRobot.type;

import com.tubes.Core.CONTROL.ATTACK;
import com.tubes.Core.CONTROL.DROPRATE;
import com.tubes.Core.CONTROL.HEALTH;
import com.tubes.Core.CONTROL.SPEED;
import com.tubes.Core.Movable;
import com.tubes.GiantRobot.GiantRobot;

public final class GRD2 extends GiantRobot implements Movable {

	private static final int width = 2;
	private static final int height = 1;
	private static final int damage = ATTACK.MEDIUM;
	private static final int speed = SPEED.HIGH;
	private static final int defend = HEALTH.LOW;
	private static final int Sparepartdrop = DROPRATE.MEDIUM;
	private int health = GRD2.defend;

	public GRD2(int x, int y) {
		super(x, y, GRD2.width, GRD2.height);
	}

	public GRD2() {
		super(0, 0, GRD2.width, GRD2.height);
	}

	public final int getDamage() {
		return GRD2.damage;
	}

	public final int getSparepartdrop() {
		return GRD2.Sparepartdrop;
	}

	@Override
	public final int getSpeed() {
		return GRD2.speed;
	}

	public int getHealth() {
		return health;
	}

	public void setHealth(int health) {
		this.health = health;
	}

	public void setLocation(int x, int y) {
	}
}
