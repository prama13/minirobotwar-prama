package com.tubes.GiantRobot.type;

import com.tubes.Core.CONTROL.ATTACK;
import com.tubes.Core.CONTROL.DROPRATE;
import com.tubes.Core.CONTROL.HEALTH;
import com.tubes.Core.CONTROL.SPEED;
import com.tubes.Core.Movable;
import com.tubes.GiantRobot.GiantRobot;

public final class GRD3 extends GiantRobot implements Movable {

	private static final int width = 1;
	private static final int height = 2;
	private static final int damage = ATTACK.LOW;
	private static final int speed = SPEED.LOW;
	private static final int defend = HEALTH.HIGH;
	private static final int Sparepartdrop = DROPRATE.MEDIUM;
	private int health = GRD3.defend;

	public GRD3(int x, int y) {
		super(x, y, GRD3.width, GRD3.height);
	}

	public GRD3() {
		super(0, 0, GRD3.width, GRD3.height);
	}

	public final int getDamage() {
		return GRD3.damage;
	}

	public final int getSparepartdrop() {
		return GRD3.Sparepartdrop;
	}

	@Override
	public final int getSpeed() {
		return GRD3.speed;
	}

	public int getHealth() {
		return health;
	}

	public void setHealth(int health) {
		this.health = health;
	}

	public void setLocation(int x, int y) {
	}
}
