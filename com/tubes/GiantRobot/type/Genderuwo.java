/*Genderuwo.java*/
package com.tubes.GiantRobot.type;

import com.tubes.Core.CONTROL.ATTACK;
import com.tubes.Core.CONTROL.DROPRATE;
import com.tubes.Core.CONTROL.HEALTH;
import com.tubes.Core.CONTROL.SPEED;
import com.tubes.Core.Movable;
import com.tubes.GiantRobot.GiantRobot;

/*Genderuwo*/
public final class Genderuwo extends GiantRobot implements Movable {

	private static final int width = 1;
	private static final int height = 1;
	private static final int damage = ATTACK.MEDIUM;
	private static final int speed = SPEED.LOW;
	private static final int defend = HEALTH.LOW;
	private static final int Sparepartdrop = DROPRATE.MEDIUM;
	private int health = Genderuwo.defend;

	public Genderuwo(int x, int y) {
		super(x, y, Genderuwo.width, Genderuwo.height);

	}

	public Genderuwo() {
		super(0, 0, Genderuwo.width, Genderuwo.height);
	}

	public final int getDamage() {

		return Genderuwo.damage;
	}

	public final int getSparepartdrop() {

		return Genderuwo.Sparepartdrop;
	}

	@Override
	public final int getSpeed() {

		return Genderuwo.speed;
	}

	public int getHealth() {
		return health;
	}

	public void setHealth(int health) {
		this.health = health;
	}

	public void setLocation(int x, int y) {
	}

}
