package com.tubes.GiantRobot.type;

import com.tubes.Core.CONTROL.ATTACK;
import com.tubes.Core.CONTROL.DROPRATE;
import com.tubes.Core.CONTROL.HEALTH;
import com.tubes.Core.CONTROL.SPEED;
import com.tubes.Core.Movable;
import com.tubes.GiantRobot.GiantRobot;

public final class GRU2 extends GiantRobot implements Movable {

	private static final int width = 1;
	private static final int height = 2;
	private static final int damage = ATTACK.LOW;
	private static final int speed = SPEED.HIGH;
	private static final int defend = HEALTH.LOW;
	private static final int Sparepartdrop = DROPRATE.MEDIUM;
	private int health = GRU2.defend;

	public GRU2(int x, int y) {
		super(x, y, GRU2.width, GRU2.height);
	}

	public GRU2() {
		super(0, 0, GRU2.width, GRU2.height);
	}

	public final int getDamage() {
		return GRU2.damage;
	}

	public final int getSparepartdrop() {
		return GRU2.Sparepartdrop;
	}

	@Override
	public final int getSpeed() {
		return GRU2.speed;
	}

	public int getHealth() {
		return health;
	}

	public void setHealth(int health) {
		this.health = health;
	}

	public void setLocation(int x, int y) {
	}
}
