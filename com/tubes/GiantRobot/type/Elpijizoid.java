package com.tubes.GiantRobot.type;

import com.tubes.Core.CONTROL.ATTACK;
import com.tubes.Core.CONTROL.DROPRATE;
import com.tubes.Core.CONTROL.HEALTH;
import com.tubes.Core.CONTROL.SPEED;
import com.tubes.Core.Movable;
import com.tubes.GiantRobot.GiantRobot;

public final class Elpijizoid extends GiantRobot implements Movable {


	private static final int width = 2;
	private static final int height = 2;

	private static final int damage = ATTACK.MEDIUM;
	private static final int speed = SPEED.MEDIUM;
	private static final int defend = HEALTH.MEDIUM;
	private static final int Sparepartdrop = DROPRATE.MEDIUM;
	private int health = Elpijizoid.defend;

	public Elpijizoid(int x, int y) {
		super(x, y, Elpijizoid.width, Elpijizoid.height);
	}

	public Elpijizoid() {
		super(0, 0, Elpijizoid.width, Elpijizoid.height);
	}

	public final int getDamage() {
		return Elpijizoid.damage;
	}

	public final int getSparepartdrop() {
		return Elpijizoid.Sparepartdrop;
	}

	@Override
	public final int getSpeed() {
		return Elpijizoid.speed;
	}

	public int getHealth() {
		return health;
	}

	public void setHealth(int health) {
		this.health = health;
	}

	public void setLocation(int x, int y) {
	}
}
