package com.tubes.MiniRobot;

public class AttackerHorizontal implements Attacker {
	@Override
	public AttackDirection shoot() {
		return AttackDirection.HORIZONTAL;
	}
}