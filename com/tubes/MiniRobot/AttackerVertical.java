package com.tubes.MiniRobot;

public class AttackerVertical implements Attacker {

	@Override
	public AttackDirection shoot() {
		return AttackDirection.VERTICAL;
	}

}