package com.tubes.MiniRobot;

import com.tubes.Player.Energy;

public interface Energizer {
	public Energy genEnergy();
}