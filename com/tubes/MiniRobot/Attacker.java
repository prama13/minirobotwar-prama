package com.tubes.MiniRobot;

public interface Attacker {
	abstract AttackDirection shoot();
}
