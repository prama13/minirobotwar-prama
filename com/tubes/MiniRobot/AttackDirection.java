package com.tubes.MiniRobot;

public enum AttackDirection {
	HORIZONTAL, DIAGONAL, VERTICAL
}
