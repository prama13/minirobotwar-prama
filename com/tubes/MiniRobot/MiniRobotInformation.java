package com.tubes.MiniRobot;

import java.lang.reflect.Field;

import com.tubes.Core.Information;

public class MiniRobotInformation extends Information {

	public String name;
	public String type;
	public String habit;
	public int attack;
	public int defend;
	public int energy;
	private boolean playable;
	public int cost;

	// protected boolean playable << ada di Superclass

	public String[] getFieldNames() {
		Class<?> info = getClass();
		Field[] field = info.getFields();

		String[] fieldNames = new String[field.length - 1];
		for (int i = 0; i < field.length - 1; i++)
			fieldNames[i] = field[i].getName();

		return fieldNames;
	}

	public void printInformation() {
		this.getClass();

		System.out.append("Nama:	" + name + "\n")
				.append("Cost:	" + energy + "\n")
				.append("Tipe:	" + type + "\n")
				.append("Habit:	" + habit + "\n")
				.append("Attack:	" + attack + "\n")
				.append("Defend:	" + defend + "\n");

	}

	public String[] getElement() {

		String[] elementList = new String[getClass().getFields().length + 1];

		elementList[0] = name;
		elementList[1] = type;
		elementList[2] = habit;
		elementList[3] = ((Integer) attack).toString();
		elementList[4] = ((Integer) defend).toString();
		elementList[5] = ((Integer) energy).toString();
		elementList[6] = ((Integer) cost).toString();
		;

		return elementList;

	}

	public void setPlayable(boolean set) {
		playable = set;
	}

	public boolean isPlayable() {
		return playable;
	}

	public static class Builder {

		private String name;
		private String type;
		private String habit;
		private int attack;
		private int defend;
		private int energy;
		private boolean playable;
		private int cost;

		public Builder name(String name) {
			this.name = name;
			return this;
		}

		public Builder type(String type) {
			this.type = type;
			return this;
		}

		public Builder habit(String habit) {
			this.habit = habit;
			return this;
		}

		public Builder attack(int attack) {
			this.attack = attack;
			return this;
		}

		public Builder defend(int defend) {
			this.defend = defend;
			return this;
		}

		public Builder energy(int energy) {
			this.energy = energy;
			return this;
		}

		public Builder playable(boolean playable) {
			this.playable = playable;
			return this;
		}

		public Builder cost(int cost) {
			this.cost = cost;
			return this;
		}

		public MiniRobotInformation build() {
			MiniRobotInformation miniRobotInformation = new MiniRobotInformation();

			miniRobotInformation.name = name;
			miniRobotInformation.type = type;
			miniRobotInformation.habit = habit;
			miniRobotInformation.attack = attack;
			miniRobotInformation.defend = defend;
			miniRobotInformation.energy = energy;
			miniRobotInformation.playable = playable;
			miniRobotInformation.cost = cost;
			return miniRobotInformation;
		}
	}
}
