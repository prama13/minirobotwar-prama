package com.tubes.MiniRobot.type;

import com.tubes.Core.Airborne;
import com.tubes.Core.CONTROL.ATTACK;
import com.tubes.Core.CONTROL.ENERGY;
import com.tubes.Core.CONTROL.HEALTH;
import com.tubes.Core.CONTROL.PRICE;
import com.tubes.MiniRobot.AttackerMiniRobot;
import com.tubes.MiniRobot.MiniRobot;

public class Bomber extends AttackerMiniRobot implements Airborne {
	public static final int ID = 5;
	public static int attack = ATTACK.HIGH;
	public static int cost = PRICE.MEDIUM;
	public static int defend = HEALTH.MEDIUM;
	public static int energy = ENERGY.MEDIUM;

	private static boolean playable;
	private int health = Bomber.defend;

	public void setHealth(int health) {
		this.health = health;
	}

	public Bomber() {
		super(0, 0);
		super.vertical();
		MiniRobot.types.add(information);
	}

	public Bomber(int x, int y) {
		super(x, y);
		super.vertical();
		MiniRobot.types.add(information);
	}

	public static void printInfo() {
		new Bomber().printInformation();
	}

	/* Getter id */
	public static int getId() {
		return Bomber.ID;
	}

	/* Getter attack */
	public int getAttack() {
		return Bomber.attack;
	}

	/* Getter cost */
	public int getCost() {
		return Bomber.cost;
	}

	/* Getter defend */
	public int getDefend() {
		return Bomber.defend;
	}

	/* Getter energy */
	public int getEnergy() {
		return Bomber.energy;
	}

	/* Getter level */

	/* Getter playable */
	public boolean isPlayable() {
		return Bomber.playable;
	}

	/* Getter health */
	public int getHealth() {
		return health;
	}

	@Override
	public void fly() {
		// TODO Auto-generated method stub

	}
}
