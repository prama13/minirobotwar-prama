package com.tubes.CLI.Exception;

public class AddMiniRobotException extends Exception {

	public AddMiniRobotException(String string) {
		super("A Mini Robot is already there!");
	}

}
