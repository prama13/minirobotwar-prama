package com.tubes.CLI;

import java.awt.Color;

import com.tubes.Core.Data;
import com.tubes.Player.Player;

import enigma.console.Console;

public final class MAIN {

	public static Console mainConsole = Konsol.enigma;

	public static void main(String[] args) {
		Konsol.color(Color.GREEN);
		Data.START();
		Player.run(args);
		
	}
}
