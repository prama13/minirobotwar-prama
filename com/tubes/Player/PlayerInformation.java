package com.tubes.Player;

import com.tubes.Core.Information;

public class PlayerInformation extends Information {

	public String ID;
	public String name;
	public int level;
	public int maxHealth;
	public Sparepart sparepart;
	public int slot;

	public static class Builder {
		private String ID;
		private String name;
		private int level;
		private int maxHealth;
		private Sparepart sparepart;
		private int slot;

		public Builder ID(String ID) {
			this.ID = ID;
			return this;
		}

		public Builder name(String name) {
			this.name = name;
			return this;
		}

		public Builder level(int level) {
			this.level = level;
			return this;
		}

		public Builder maxHealth(int maxHealth) {
			this.maxHealth = maxHealth;
			return this;
		}

		public Builder sparepart(Sparepart sparepart) {
			this.sparepart = sparepart;
			return this;
		}

		public Builder slot(int slot) {
			this.slot = slot;
			return this;
		}

		public PlayerInformation build() {
			PlayerInformation playerInformation = new PlayerInformation();
			playerInformation.ID = ID;
			playerInformation.name = name;
			playerInformation.level = level;
			playerInformation.maxHealth = maxHealth;
			playerInformation.sparepart = sparepart;
			playerInformation.slot = slot;
			return playerInformation;
		}
	}

	@Override
	public String[] getElement() {

		return null;
	}

	public void printInformation() {

		System.out.append("Nama:		" + name + "\n")
				.append("Level:		" + level + "\n")
				.append("Max Health:	" + maxHealth + "\n")
				.append("Sparepart:	" + sparepart.getValue() + " $p\n")
				.append("Slot:		" + slot + "\n")

		;

	}
}
