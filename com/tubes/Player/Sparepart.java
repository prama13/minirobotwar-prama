package com.tubes.Player;

public class Sparepart {
	private int value;

	public Sparepart() {
		value = 10;
	}

	public Sparepart(int value) {
		this.value = value;
	}

	public void add(int i) {
		setValue(getValue() + i);
	}

	public void add(Sparepart other) {
		value += other.value;
		try {
			other.finalize();
		} catch (Throwable e) {
			e.printStackTrace();
		}
	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}

}