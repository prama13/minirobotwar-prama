package com.tubes.Player;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

import org.apache.commons.lang3.StringUtils;

public class Savable implements Serializable {

	public void Save() {
		try {

			FileOutputStream file = new FileOutputStream(
					StringUtils.uncapitalize("lalala.")
							+ this.getClass().getSimpleName());
			ObjectOutputStream saving = new ObjectOutputStream(file);

			saving.writeObject(this);

			saving.close();
			file.close();
			System.out.println("\nSaved Successfully!");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public Object load() {
		Object load = new Object();
		try {

			System.out.println("\nLoading File....");
			FileInputStream fileout = new FileInputStream(this.getClass()
					.getSimpleName());
			ObjectInputStream loading = new ObjectInputStream(fileout);

			load = loading.readObject();

			loading.close();
			fileout.close();

		} catch (FileNotFoundException e) {
			System.err.println(this.getClass().getSimpleName()
					+ "tidak ditemukan");// TODO Tambahin Syntax penamaan
											// berdasarkan nama Class+UID
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		return load;
	}
}