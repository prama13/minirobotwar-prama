package com.tubes.Player;

import java.util.HashMap;
import java.util.Map;

/**
 * A complex implementation using the most cutting edge database technology to
 * store users.
 * 
 * @author Sean
 * 
 */
public class PlayerDatastore {

	private Map<String, Player> _database;

	public PlayerDatastore() {
		_database = new HashMap<String, Player>();
	}

	/**
	 * Add a player to the datastore.
	 * 
	 * @param player
	 *            The player to add.
	 * @return The previous player that was replaced in the database, or null if
	 *         there was none.
	 */
	public Player addPlayer(Player player) {
		return _database.put(createKey(player), player);
	}

	/**
	 * Get a player from the database.
	 * 
	 * @param ID
	 *            The first name.
	 * @param lastName
	 *            The last name.
	 * @return The player, or null if not found.
	 */
	public Player getPlayer(String ID) {
		return _database.get(createKey(ID));
	}

	/**
	 * Generate a key for the player.
	 * 
	 * @param u
	 *            The player to generate a key for.
	 * @return The "unique" key.
	 */
	private String createKey(Player u) {
		return createKey(u.getID());
	}

	/**
	 * We don't support people of the same name but different ages. Deal with
	 * it.
	 * 
	 * @param ID
	 *            The player's ID
	 * @param Name
	 *            The player's Name
	 * @return The "unique" key.
	 */
	private String createKey(String ID) {
		return ID;
	}
}
