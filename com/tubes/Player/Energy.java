package com.tubes.Player;

public class Energy {

	private int value;

	public Energy(int value) {
		super();
		this.value = value;
	}

	public void add(Energy other) {
		value += other.value;
		try {
			other.finalize();
		} catch (Throwable e) {
			e.printStackTrace();
		}
	}

	public void add(int i) {
		value += i;
	}

	public int getValue() {
		return value;
	}

}