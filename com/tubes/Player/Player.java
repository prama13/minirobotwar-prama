package com.tubes.Player;

import java.awt.Color;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Scanner;

import org.apache.commons.lang3.StringUtils;

import com.tubes.CLI.Konsol;
import com.tubes.Core.CONTROL;
import com.tubes.MiniRobot.MiniRobot;
import com.tubes.MiniRobot.MiniRobotInformation;
import com.tubes.Player.Shop.Shop;
import com.tubes.util.Tabel;

public class Player extends Savable implements Serializable {

	private static Integer lastID = 1;
	private static final long serialVersionUID = -2456153286695704755L;

	// attribute
	private String ID;
	private String name;
	private int level;
	private int maxHealth;
	public ArrayList<String> MiniRobotNameList = new ArrayList<String>();
	private Sparepart sparepart;
	private int slot;
	public PlayerInformation information;

	// constructor
	public Player(String name) {
		super();
		this.name = name;
		ID = Player.lastID.toString();
		level = 1;
		maxHealth = 3;
		MiniRobotNameList.add("Meriam");
		MiniRobotNameList.add("Mentari");
		sparepart = new Sparepart(10);
		information = new PlayerInformation.Builder().level(level)
				.maxHealth(maxHealth).slot(slot).sparepart(sparepart)
				.name(name).ID(ID).build();
		Player.lastID++;
		new Tabel.Builder().fromList(getMiniRobotList()).end();
		 // Tiap instansiasi langsung ngesave
	}

	public Player() {
		super();
		ID = Player.lastID.toString();
		name = CONTROL.DEFAULT_NAME + " no " + ID;
		level = 1;
		maxHealth = 3;
		MiniRobotNameList.add("Meriam");
		MiniRobotNameList.add("Mentari");
		MiniRobotNameList.add("Tameng");
		sparepart = new Sparepart(10);
		slot = 3;

		information = new PlayerInformation.Builder().level(level)
				.maxHealth(maxHealth).slot(slot).sparepart(sparepart)
				.name(name).ID(ID).build();

		Player.lastID++;
	}

	// getter
	public int getLevel() {
		return level;
	}

	public int getMaxHealth() {
		return maxHealth;
	}

	public Sparepart getSparepart() {
		return sparepart;
	}

	public String getName() {
		return name;
	}

	public ArrayList<MiniRobotInformation> getMiniRobotList() {
		ArrayList<MiniRobotInformation> custom = MiniRobot.types;

		for (MiniRobotInformation info : custom)
			for (String name : MiniRobotNameList)
				if (StringUtils.equalsIgnoreCase(info.name, name))
					info.setPlayable(true);

		return custom;
	}

	public void addPlayerMiniRobot(String[] mrNames) {
		for (String name : mrNames)
			MiniRobotNameList.add(name);
	}

	public int getSlot() {
		return slot;
	}

	public PlayerInformation getInformation() {
		return information;
	}

	public String getID() {
		return ID;
	}

	// setter
	public void setLevel(int level) {
		this.level = level;
		information.level = level;
	}

	public void setMaxHealth(int maxHealth) {
		this.maxHealth = maxHealth;
		information.maxHealth = maxHealth;
	}

	public void setSparepart(Sparepart sparepart) {
		this.sparepart = sparepart;
		information.sparepart = sparepart;
	}

	public void setName(String name) {
		this.name = name;
		information.name = name;
	}

	public void setSlot(int slot) {
		this.slot = slot;
		information.slot = slot;
	}

	/**
	 * JANGAN DIPAKE!!
	 **/
	public void setID(String iD) {
		ID = iD;
		information.ID = iD;
	}

	public void printStatus() {
		information.printInformation();
		System.out.println("MiniRobot :");
		new Tabel.Builder().fromList(getMiniRobotList()).end().print();
	}

	private static enum PlayerParser {
		LAB, SHOWSTAT, PLAY, ERROR, EXIT;

		static PlayerParser parse(String[] t) {
			try {
				String s = " ";
				try {
					s = t[0].toUpperCase();
				} catch (ArrayIndexOutOfBoundsException i) {
				}
				return PlayerParser.valueOf(s);
			} catch (IllegalArgumentException ignore) {
				return ERROR;
			}

		}
	}

	public static void run(String[] args) {

		boolean state = true;
		Scanner input = new Scanner(System.in);
		while (state) {
			Konsol.color(Color.red);
			/** Delay Pointer **/
			try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
			}

			System.out.print(CONTROL.CURRENT_PLAYER.name + "> ");
			String Input = input.nextLine();
			if (Input.equalsIgnoreCase("changename")) {
				System.out.print("Enter new name: ");
				Scanner name = new Scanner(System.in);
				CONTROL.CURRENT_PLAYER.name = name.nextLine();
			} else {
				PlayerParser option = PlayerParser.parse(Input.split(" "));
				switch (option) {
				case SHOWSTAT:
					CONTROL.CURRENT_PLAYER.printStatus();
					break;

				case PLAY:
					System.err.append("\nOn Progress..\n\n");
					break;
				case LAB:
					Shop.run(args);
					break;
				case ERROR:
					Konsol.error(Input, PlayerParser.class);
					break;
				case EXIT:
					state = false;
					break;
				}
			}
		}

	}
public static void main(String[] args) {
	Player a = new Player("Hello");
	a.Save();
}
}