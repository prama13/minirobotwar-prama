package com.tubes.Player.Shop;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Scanner;

import com.tubes.CLI.Konsol;
import com.tubes.Core.CONTROL;

import com.tubes.Player.Player;
import com.tubes.util.Tabel;

public abstract class Shop {
	private static Player customer = CONTROL.CURRENT_PLAYER;

	public Player getCustomer() {
		return Shop.customer;
	}

	public static void setCustomer(Player newcustomer) {
		Shop.customer = newcustomer;
	}


	public static void printMiniRobot() {
		Tabel miniRobot = new Tabel.Builder().fromList(
				Shop.customer.getMiniRobotList()).end();
		miniRobot.addFieldNames("cost");
		miniRobot.Invert();
		miniRobot.print();
	}



	public abstract void printLive();

	public void buySlot(int i) {
		Shop.customer.setSlot(Shop.customer.getSlot() + i);
	}

	private static enum LabParser {
		LOOK, BUY, ERROR, OUT;

		static LabParser parse(String[] t) {
			try {

				String s = t[0].toUpperCase();

				return LabParser.valueOf(s);
			} catch (IllegalArgumentException ignore) {
				return ERROR;
			}

		}
	}

	public static void run(String[] args) {
		boolean state = true;
		Scanner input = new Scanner(System.in);
		Konsol.color(Color.CYAN.brighter());
		Konsol.center("WELL HELLO THERE! \n");
		Konsol.wait(700);
		Konsol.center("WELCOME TO \"THE\" LABRORATORY");
		Konsol.wait(500);
		Konsol.center("TAKE A LOOK AROUND! \n\n");

		while (state) {
			Konsol.color(Color.CYAN.brighter());
			try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
			}

			System.out.print(CONTROL.CURRENT_PLAYER.getName() + "\\LAB> ");
			String Input = input.nextLine();
			LabParser option = LabParser.parse(Input.split(" "));
			switch (option) {
			case ERROR:
				Konsol.error(Input, LabParser.class);

				break;
			case LOOK:
				Shop.printMiniRobot();
				break;
			case OUT:
				Konsol.center("OUT ALREADY?\n");
				Konsol.wait(700);
				Konsol.center("OKAY THEN, BYE!");

				state = false;
			}

		}
	}

}